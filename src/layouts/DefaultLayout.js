import React, { Fragment } from 'react';
import DefaultHeader from './DefaultHeader';

import {
  Route
} from 'react-router-dom';

export const DefaultLayout = ({ children, ...rest }) => {
  return (
    <Fragment>
      <div className="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">
        <DefaultHeader/>

        <section id="main">{children}</section>
      </div>
    </Fragment>
  )
};

export const DefaultRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={matchProps => (
      <DefaultLayout>
        <Component {...matchProps} />
      </DefaultLayout>
    )} />
  )
};