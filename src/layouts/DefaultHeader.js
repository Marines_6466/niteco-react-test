import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

const HeaderMenuLink = ({ label, to, activeOnlyWhenExact }) => (
  <Route
    path={to}
    exact={activeOnlyWhenExact}
    children={({ match }) => (
      <li className={match ? 'uk-active' : ''}>
        <Link to={to}>{label}</Link>
      </li>
    )} />
);

class DefaultHeader extends Component {
  render() {
    return (
      <nav className="uk-navbar uk-margin-large-bottom">
        <Link className="uk-navbar-brand uk-hidden-small" to="/">Brand</Link>
        <ul className="uk-navbar-nav uk-hidden-small">
          <HeaderMenuLink to="/" label="Home" activeOnlyWhenExact/>
          <HeaderMenuLink to="/feature" label="Feature"/>
        </ul>
        <div className="uk-navbar-brand uk-navbar-center uk-visible-small">Brand</div>
      </nav>
    )
  }
}

export default DefaultHeader