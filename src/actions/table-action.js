import client from '../utils/api-call';

import {
  FETCH_POPULATION, FETCH_POPULATION_SUCCESS, FETCH_POPULATION_FAILED, SELECTED_POPULATION
} from '../actions/types';

const endPoint = '/populations';

export const fetchPopulations = () => {
  return (dispatch) => {
    dispatch({ type: FETCH_POPULATION });

    client.get(endPoint)
      .then(response => {
        dispatch({ type: FETCH_POPULATION_SUCCESS, payload: response.data })
      })
      .catch(error => {
        dispatch({ type: FETCH_POPULATION_FAILED, payload: error })
      })
  }
};

export const fetchDataSelectedPopulation = (item) => {
  return (dispatch) => {
    dispatch({ type: SELECTED_POPULATION, payload: item })
  }
};