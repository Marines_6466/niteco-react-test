import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

// Layout
import {
  DefaultRoute
} from './layouts/DefaultLayout'

import Home from './containers/Home'
import Feature from './containers/Feature'

class App extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <Switch>
            <DefaultRoute exact path="/" component={Home} />
            <DefaultRoute path="/feature" component={Feature} />
          </Switch>
        </Fragment>
      </Router>
    );
  }
}

export default App;
