import React, {Component, Fragment} from 'react';

class HomeComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      populations: [],
      sort: {
        column: null,
        type: 'desc'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    // Receive Populations Data Asynchronously
    const { populations } = nextProps;

    if (populations) {
      this.setState({
        populations
      })
    }
  }

  onSort = (column) => {
    const type = this.state.sort.column ? (this.state.sort.type === 'asc' ? 'desc' : 'asc') : 'desc';
    const sortedData = this.state.populations.sort((a, b) => {
      if (column === 'cityName') {
        const addressA = a.address.toUpperCase();
        const addressB = b.address.toUpperCase();

        if (addressA < addressB) {
          return -1;
        } else if (addressA > addressB) {
          return 1;
        } else {
          return 0;
        }
      }  else {
        return 0;
      }
    });

    if (type === 'desc') {
      sortedData.reverse()
    }

    // Update Data Populations sortable
    this.setState({
      populations: sortedData,
      sort: {
        column,
        type
      }
    })
  };

  setArrow = (column) => {
    if (this.state.sort.column === column) {
      return (this.state.sort.type === 'asc') ? <i className="uk-icon-long-arrow-down ml-5"></i> : <i className="uk-icon-long-arrow-up ml-5"></i>
    }
  };

  handleSelectChart = (itemObj) => {
    this.props.fetchDataSelectedPopulation(itemObj)
  };

  renderPopulations = () => {
    const { populations } = this.state;

    if (populations) {
      return populations.map((item, index) => {
        let className = '';

        if(item === this.props.selectedPopulation) {
          className += 'selected'
        }

        return (
          <tr className={`uk-table-middle ${className}`} key={index + 1}>
            <td>{item.address}</td>
            <td>
              {item.data.map((popObj, index) => {
                return (
                  <div key={index + 1} className="uk-grid uk-grid-small">
                    <div className="uk-width-1-2">{`${popObj.min_age} - ${popObj.max_age}`}</div>
                    <div className="uk-width-1-2">{popObj.total_population}</div>
                  </div>
                )
              })}
            </td>
            <td className="uk-width-1-6">
              <button type="button" className="uk-button uk-button-primary" onClick={() => this.handleSelectChart(item)}>
                <i className="uk-icon-bar-chart"></i> Chart info
              </button>
            </td>
          </tr>
        )
      })
    } else {
      return <h4><i>No data avaiable.</i></h4>
    }
  };

  render() {
    return (
      <Fragment>
        <table className="uk-table uk-table-hover uk-table-bordered population-table">
          <thead>
          <tr>
            <th onClick={() => this.onSort('cityName')}>
              City Name
              {this.setArrow('cityName')}
            </th>
            <th>
              <div className="uk-grid uk-grid-small">
                <div className="uk-width-1-2">Age range</div>
                <div className="uk-width-1-2">Total people</div>
              </div>
            </th>
            <th>Chart</th>
          </tr>
          </thead>
          <tbody>
            {this.renderPopulations()}
          </tbody>
        </table>
      </Fragment>
    )
  }
}

export default HomeComponent