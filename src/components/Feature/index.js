import React, { Component, Fragment } from 'react';

class FeatureComponent extends Component {
  render() {
    return (
      <Fragment>
        <h4>Feature Page</h4>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cupiditate dolor dolores error facere illum inventore itaque laboriosam molestiae, necessitatibus non, omnis quaerat quibusdam saepe sed soluta suscipit, tenetur veniam?
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae ducimus eum illo nihil officia quod ratione sunt ullam? Deleniti itaque minus nisi, nostrum omnis pariatur quos ratione repellendus repudiandae soluta.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, ea error excepturi exercitationem explicabo facilis harum libero magnam nam natus nihil numquam pariatur qui reprehenderit suscipit tenetur, ut vero voluptatibus!
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci nihil praesentium quae qui quisquam. Delectus labore magni molestias numquam possimus quaerat quas qui reiciendis sed voluptas. Molestias necessitatibus quibusdam quod.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus architecto fugit harum labore laudantium maiores non officia placeat porro quasi quis quod quos sed, sint tenetur vel, voluptas! Vel!
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. A est fuga sequi! Facilis incidunt itaque labore laudantium quos recusandae unde voluptatibus! Adipisci, dolore ipsam ipsum magni minus rerum totam. In!
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci corporis cumque debitis delectus deleniti doloribus itaque labore placeat quod vitae. Aliquam aperiam dolore inventore non quae quia rem sunt voluptate.
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi asperiores cumque, dolores eos, facilis fugiat fugit id illum magni officiis pariatur, perspiciatis vel veniam vero voluptatum. Eveniet rerum vero voluptate.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis commodi distinctio earum eligendi esse exercitationem hic, illo ipsa laborum minus modi obcaecati quam qui quis, sequi sint soluta totam?
        </p>
      </Fragment>
    )
  }
}

export default FeatureComponent