import React, {Component} from 'react'
import '../../assets/scss/styles.css';

class ChartComponent extends Component {

  render() {
    const {populationData} = this.props;
    const colors = ['#43A19E', '#7B43A1', '#F2317A', '#FF9824'];

    if (populationData && populationData.data !== undefined) {
      let sum = 0;
      populationData.data.map((item) => {
        sum += item.total_population;
        return item;
      });

      return (
        <div className="wrap-chart-series">
          <div className="chart-serie">
            <label>{populationData.address}</label>
            {
              populationData.data.map((item, idx) => {
                return (
                  <div className="chart-serie__item" key={idx}
                       style={{height: (item.total_population * 100 / sum) + '%', backgroundColor: colors[idx]}}>
                    <b style={{color: colors[idx]}}>{item.total_population}</b>
                  </div>
                )
              })
            }
          </div>

          <div className="wrap-legend">
            <div className="uk-grid uk-flex-middle">
              <div className="uk-width-auto">
                Age
              </div>
              <div className="uk-width-expand">
                <div className="uk-grid uk-grid-small">
                  <div className="uk-width-1-4">
                    <div style={{backgroundColor: `${colors[0]}`, textAlign: 'center', color: '#fff'}}>1 - 10</div>
                  </div>
                  <div className="uk-width-1-4">
                    <div style={{backgroundColor: `${colors[1]}`, textAlign: 'center', color: '#fff'}}>11 - 20</div>
                  </div>
                  <div className="uk-width-1-4">
                    <div style={{backgroundColor: `${colors[2]}`, textAlign: 'center', color: '#fff'}}>21 - 30</div>
                  </div>
                  <div className="uk-width-1-4">
                    <div style={{backgroundColor: `${colors[3]}`, textAlign: 'center', color: '#fff'}}>31 - 40</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div></div>
    )

  }
}

export default ChartComponent