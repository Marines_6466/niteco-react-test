import {
  FETCH_POPULATION, FETCH_POPULATION_SUCCESS, FETCH_POPULATION_FAILED, SELECTED_POPULATION
} from '../actions/types'

const initialState = {
  populations: [],
  selectedPopulation: null,
  loading: false
};

const table = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POPULATION:
      return {
        ...state,
        loading: true
      };

    case FETCH_POPULATION_SUCCESS:
      return {
        ...state,
        populations: action.payload,
        selectedPopulation: action.payload[0],
        loading: false
      };

    case FETCH_POPULATION_FAILED:
      return {
        ...state,
        loading: false
      };

    case SELECTED_POPULATION:
      return {
        ...state,
        selectedPopulation: action.payload
      };

    default:
      return state
  }
};

export default table