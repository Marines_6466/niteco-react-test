import { combineReducers } from 'redux';
import table from './table-reducer'

const rootReducer = combineReducers({
  table
});

export default rootReducer