import React, { Component, Fragment } from 'react';
import FeatureComponent from '../../components/Feature'

class FeatureContainer extends Component {
  render() {
    return (
      <Fragment>
        <FeatureComponent/>
      </Fragment>
    )
  }
}

export default FeatureContainer