import React, { Component, Fragment } from 'react';
import '../../assets/scss/styles.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import HomeComponent from '../../components/Home';
import ChartComponent from '../../components/Chart';

import { fetchPopulations, fetchDataSelectedPopulation } from '../../actions/table-action';

class HomeContainer extends Component {
  componentWillMount() {
    document.title = 'Populations';
    this.props.fetchPopulations()
  }

  render() {
    const { populations, selectedPopulation, fetchDataSelectedPopulation } = this.props;

    return (
      <Fragment>
        <ChartComponent populationData={selectedPopulation}/>

        <HomeComponent
          populations={populations}
          selectedPopulation={selectedPopulation}
          fetchDataSelectedPopulation={fetchDataSelectedPopulation} />
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    populations           : state.table.populations,
    selectedPopulation    : state.table.selectedPopulation
  }
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    fetchPopulations,
    fetchDataSelectedPopulation
  }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)